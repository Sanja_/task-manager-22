package ru.karamyshev.taskmanager.command.task;

import org.jetbrains.annotations.NotNull;
import ru.karamyshev.taskmanager.command.AbstractCommand;
import ru.karamyshev.taskmanager.endpoint.Role;
import ru.karamyshev.taskmanager.endpoint.Session;
import ru.karamyshev.taskmanager.endpoint.Task;
import ru.karamyshev.taskmanager.util.TerminalUtil;


public class TaskRemoveByIndexCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-tskrmvind";
    }

    @NotNull
    @Override
    public String name() {
        return "task-remove-by-index";
    }

    @Override
    public @NotNull String description() {
        return "Remove task by index.";
    }

    @NotNull
    @Override
    public void execute() throws Exception {
        final Session session = serviceLocator.getSessionService().getSession();
        System.out.println("[REMOVE TASK]");
        System.out.println("ENTER TASK INDEX FOR DELETION:");
        final Integer index = TerminalUtil.nextNumber();
        final Task task = serviceLocator.getTaskEndpoint().removeTaskOneByIndex(session, index);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @NotNull
    public Role[] roles() {
        return new Role[]{Role.USER, Role.ADMIN};
    }

}
