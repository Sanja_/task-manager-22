package ru.karamyshev.taskmanager.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.entity.Session;
import ru.karamyshev.taskmanager.entity.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface ITaskEndpoint {

    @WebMethod
    void createName(
            @Nullable Session session,
            @Nullable String name
    ) throws Exception;

    @WebMethod
    void createDescription(
            @Nullable Session session,
            @Nullable String name,
            @Nullable String description
    ) throws Exception;

    @WebMethod
    void addTask(
            @Nullable Session session,
            @Nullable Task task
    ) throws Exception;

    @WebMethod
    void removeTask(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "task", partName = "task") @Nullable Task task
    ) throws Exception;

    @Nullable
    @WebMethod
    List<Task> findAllTask(
            @WebParam(name = "session", partName = "session") @Nullable Session session
    ) throws Exception;

    @Nullable
    @WebMethod
    void clearTask(
            @WebParam(name = "session", partName = "session") @Nullable Session session
    ) throws Exception;

    @Nullable
    @WebMethod
    Task findTaskOneById(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "taskId", partName = "taskId") @Nullable String id
    ) throws Exception;

    @Nullable
    @WebMethod
    Task findTaskOneByIndex(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "taskIndex", partName = "taskIndex") @Nullable Integer index
    ) throws Exception;

    @Nullable
    @WebMethod
    List<Task> findTaskOneByName(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "taskName", partName = "taskName") @Nullable String name
    ) throws Exception;

    @Nullable
    @WebMethod
    Task removeTaskOneById(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "taskId", partName = "taskId") @Nullable String id
    ) throws Exception;

    @Nullable
    @WebMethod
    Task removeTaskOneByIndex(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "taskIndex", partName = "taskIndex") @Nullable Integer index
    ) throws Exception;

    @Nullable
    @WebMethod
    List<Task> removeTaskOneByName(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "taskName", partName = "taskName") @Nullable String name
    ) throws Exception;

    @Nullable
    @WebMethod
    Task updateTaskById(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "taskId", partName = "taskId") @Nullable String id,
            @WebParam(name = "taskName", partName = "taskName") @Nullable String name,
            @WebParam(name = "taskDescription", partName = "taskDescription") @Nullable String description
    ) throws Exception;

    @Nullable
    @WebMethod
    Task updateTaskByIndex(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "taskIndex", partName = "taskIndex") @Nullable Integer index,
            @WebParam(name = "taskName", partName = "taskName") @Nullable String name,
            @WebParam(name = "taskDescription", partName = "taskDescription") @Nullable String description
    ) throws Exception;

    @Nullable
    @WebMethod
    List<Task> getTaskList(
            @WebParam(name = "session", partName = "session") @Nullable Session session
    ) throws Exception;

    @WebMethod
    void loadTask(
            @WebParam(name = "session", partName = "session") @Nullable Session session,
            @WebParam(name = "tasks", partName = "tasks") @Nullable List<Task> tasks
    ) throws Exception;

}
